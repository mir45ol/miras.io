/**
 * Image Builder Configuration
 * @typedef {Object} ImageBuilderConfig
 * @property {Object.<string, Array.<SharpOpts>>} sharpOpts Sharp options
 * @property {ChokidarOpts} chokidarOpts Chokidar options
 */

/**
 * Sharp Options
 * @typedef {Object} SharpOpts
 * @property {string} buildFile
 * @property {number} width
 * @property {number} height
 */

/**
 * Chokidar Options
 * @typedef {Object} ChokidarOpts
 * @property {ChokidarWatchOpts} watchOpts Chokidar watch options
 */

/**
 * Chokidar Watch Options
 * See the [official documentation]{@link https://github.com/paulmillr/chokidar/tree/3409db854565caeb17f87d1e5492a507c2a9103e#api}.
 * @typedef {Object} ChokidarWatchOpts
 * @property {boolean} [persistent]
 * @property {*} [ignored]
 * @property {boolean} [ignoreInitial]
 * @property {boolean} [followSymlinks]
 * @property {string} [cwd]
 * @property {boolean} [disableGlobbing]
 * @property {boolean} [usePolling]
 * @property {boolean} [useFsEvents]
 * @property {boolean} [alwaysStat]
 * @property {number} [depth]
 * @property {number} [interval]
 * @property {number} [binaryInterval]
 * @property {boolean} [ignorePermissionErrors]
 * @property {boolean|number} [atomic]
 * @property {ChokidarAwaitWriteFinishOpts|boolean} [awaitWriteFinish]
 */

/**
 * Chokidar Await Write Finish Options
 * @typedef {Object} ChokidarAwaitWriteFinishOpts
 * @property {number} [stabilityThreshold]
 * @property {number} [pollInterval]
 */

const
  PATH = require('path'),
  SHARP = require('sharp'),
  CHOKIDAR = require('chokidar'),
  JOBS_PROMISE = require('./JobsPromise'),
  MKDIR = require('./mkdir'),
  LOG = require('./log');

const LOG_MODULE = 'Image Builder';

/**
 * Build images.
 * @param {ImageBuilderConfig} config The image builder configuration
 * @returns {Promise} A Promise with an Error on rejection.
 */
function build(config) {

  /* Return a Promise so we can use JavaScript's async & await features. */
  return new Promise((resolve, reject) => {

    /* Create an empty list of jobs. */
    const JOBS = [];

    /* Populate the list of jobs. */
    for (const SRC_FILE in config.sharpOpts) {
      config.sharpOpts[SRC_FILE].forEach(sharpOpts => {
        JOBS.push(`${SRC_FILE} => ${sharpOpts.buildFile}`);
      });
    }

    /* Create a JobsPromise. */
    const _JOBS_PROMISE = new JOBS_PROMISE(JOBS, resolve, reject);

    for (const SRC_FILE in config.sharpOpts) {
      config.sharpOpts[SRC_FILE].forEach(sharpOpts => {

        /* Make the directory where the image will be written to. */
        const DIRNAME = PATH.dirname(sharpOpts.buildFile);
        MKDIR(LOG_MODULE, DIRNAME).then(
          () => {

            /* If the directory was made without Error,
             * Log what we're about to do. */
            LOG.logWarning(LOG_MODULE, `Building ${SRC_FILE} => ${sharpOpts.buildFile}`);
            SHARP(SRC_FILE)
              .resize(sharpOpts.width, sharpOpts.height)
              .toFile(sharpOpts.buildFile)
              .then(
                () => {

                  /* Log what we did & resolve the JobsPromise. */
                  LOG.logSuccess(LOG_MODULE, `Built ${SRC_FILE} => ${sharpOpts.buildFile}`);
                  _JOBS_PROMISE.resolve(`${SRC_FILE} => ${sharpOpts.buildFile}`);
                },

                sharp_onrejected => {

                  /* Log what we couldn't do, the Error's message,
                   * & reject the JobsPromise with the Error. */
                  LOG.logError(LOG_MODULE, `Couldn't build ${SRC_FILE} => ${sharpOpts.buildFile}`);
                  LOG.logError(LOG_MODULE, sharp_onrejected.message);
                  _JOBS_PROMISE.reject(`${SRC_FILE} => ${sharpOpts.buildFile}`, sharp_onrejected);
                }
              );
          },

          /* If the directory couldn't be made,
           * reject the JobsPromise with the Error. */
          mkdir_onrejected => _JOBS_PROMISE.reject(`${SRC_FILE} => ${sharpOpts.buildFile}`, mkdir_onrejected)
        );
      });
    }
  });
}

function configAddPaths(config, configCpy, paths) {
  if (typeof paths === 'string') {
    paths = [paths];
  }

  paths.forEach(path => {
    configCpy.sharpOpts[path] = config.sharpOpts[path];
  });

  return configCpy;
}

function configChangePaths(config, paths) {
  if (typeof paths === 'string') {
    paths = [paths];
  }

  const CONFIG_CPY = {
    sharpOpts: {},
    chokidarOpts: config.chokidarOpts,
  };

  paths.forEach(path => {
    CONFIG_CPY.sharpOpts[path] = config.sharpOpts[path];
  });

  return CONFIG_CPY;
}

function configUnlinkPaths(config, configCpy, paths) {
  if (typeof paths === 'string') {
    paths = [paths];
  }

  paths.forEach(path => {
    delete configCpy.sharpOpts[path];
  });

  return configCpy;
}

/**
 * Gracefully build images (handle Promise resolution & rejection internally).
 * @param {ImageBuilderConfig} config The image builder configuration
 */
function gracefullyBuild(config) {
  build(config).then(
    () => {},
    () => {}
  );
}

/**
 * Watch & gracefully build images.
 * @param {ImageBuilderConfig} config The image builder configuration
 */
function watch(config) {
  const CONFIG_CPY = {
    sharpOpts: {},
    chokidarOpts: config.chokidarOpts,
  };

  const SRC_FILES = Object.keys(config.sharpOpts);

  CHOKIDAR.watch(SRC_FILES, config.chokidarOpts.watchOpts)
    .on('add', paths => gracefullyBuild(configAddPaths(config, CONFIG_CPY, paths)))
    .on('change', paths => gracefullyBuild(configChangePaths(config, paths)))
    .on('unlink', paths => gracefullyBuild(configUnlinkPaths(config, CONFIG_CPY, paths)));
}

exports.build = build;
exports.watch = watch;
