const
  PATH = require('path'),
  ROLLUP = require('rollup'),
  MKDIR = require('./mkdir');

const LOG_MODULE = 'JS Bundler';

function prebuild(config) {
  const DIR = PATH.dirname(config.outputOpts.file);
  return MKDIR(LOG_MODULE, DIR);
}

async function build(config) {
  const BUNDLE = await ROLLUP.rollup(config.inputOpts);
  await BUNDLE.write(config.outputOpts);
}

function watch(config) {
  const WATCH_OPTS = Object.assign({
    output: config.outputOpts,
    watch: config.watchOpts,
  }, config.inputOpts);

  const WATCHER = ROLLUP.watch(WATCH_OPTS);

  WATCHER.on('event', event => {
    if (event.code === 'FATAL') {
      WATCHER.close();
      process.exit(1);
    }
  });
}

exports.prebuild = prebuild;
exports.build = build;
exports.watch = watch;
