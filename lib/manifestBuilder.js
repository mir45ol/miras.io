/**
 * Manifest Builder Configuration
 * @typedef {Object} ManifestBuilderConfig
 * @property {string} srcFile
 * @property {string} buildFile
 * @property {JsonOpts} jsonOpts JSON options
 * @property {ChokidarOpts} chokidarOpts Chokidar options
 */

/**
 * JSON Options
 * See the [official documentation]{@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify#Parameters}.
 * @typedef {Object} JsonOpts
 * @property {Function|Array.<string|number>} [replacer]
 * @property {string|number} [space]
 */

/**
 * Chokidar Options
 * @typedef {Object} ChokidarOpts
 * @property {ChokidarWatchOpts} watchOpts Chokidar watch options
 */

/**
 * Chokidar Watch Options
 * See the [official documentation]{@link https://github.com/paulmillr/chokidar/tree/3409db854565caeb17f87d1e5492a507c2a9103e#api}.
 * @typedef {Object} ChokidarWatchOpts
 * @property {boolean} [persistent]
 * @property {*} [ignored]
 * @property {boolean} [ignoreInitial]
 * @property {boolean} [followSymlinks]
 * @property {string} [cwd]
 * @property {boolean} [disableGlobbing]
 * @property {boolean} [usePolling]
 * @property {boolean} [useFsEvents]
 * @property {boolean} [alwaysStat]
 * @property {number} [depth]
 * @property {number} [interval]
 * @property {number} [binaryInterval]
 * @property {boolean} [ignorePermissionErrors]
 * @property {boolean|number} [atomic]
 * @property {ChokidarAwaitWriteFinishOpts|boolean} [awaitWriteFinish]
 */

/**
 * Chokidar Await Write Finish Options
 * @typedef {Object} ChokidarAwaitWriteFinishOpts
 * @property {number} [stabilityThreshold]
 * @property {number} [pollInterval]
 */

const
  PATH = require('path'),
  CHOKIDAR = require('chokidar'),
  MKDIR = require('./mkdir'),
  READ_FILE = require('./readFile'),
  WRITE_FILE = require('./writeFile'),
  LOG = require('./log');

const LOG_MODULE = 'Manifest Builder';

/**
 * Make the directory where the manifest will be written to.
 * @param {ManifestBuilderConfig} config The manifest builder configuration.
 * @returns {Promise} A Promise with an Error on rejection.
 */
function prebuild(config) {
  const DIRNAME = PATH.dirname(config.buildFile);
  return MKDIR(LOG_MODULE, DIRNAME);
}

/**
 * Build the manifest.
 * @param {ManifestBuilderConfig} config The manifest builder configuration.
 * @returns {Promise} A Promise with an Error on rejection.
 */
function build(config) {

  /* Return a Promise so we can use JavaScript's async & await features. */
  return new Promise((resolve, reject) => {

    /* Read the source manifext. */
    READ_FILE(LOG_MODULE, config.srcFile).then(
      readFile_fulfilled => {

        /* If the source manifest was read without Error,
         * process the JSON & write it to the build manifest. */

        let
          manifestObj,
          manifestStr;

        try {

          /* Log what we're about to do. */
          LOG.logWarning(LOG_MODULE, 'Processing JSON');
          manifestObj = JSON.parse(readFile_fulfilled);
          manifestStr = JSON.stringify(manifestObj, config.jsonOpts.replacer, config.jsonOpts.space);

          /* Log what we did. */
          LOG.logSuccess(LOG_MODULE, 'Processed JSON');

        } catch (err) {

          /* Log what we couldn't do, the Error's message,
           * & reject the Promise with the Error. */
          LOG.logError(LOG_MODULE, 'Couldn\'t process JSON');
          LOG.logError(LOG_MODULE, err.message);
          reject(err);

        }

        WRITE_FILE(LOG_MODULE, config.buildFile, manifestStr).then(

          /* If the build manifest was written without Error,
           * resolve the Promise. */
          writeFile_fulfilled => resolve(writeFile_fulfilled),

          /* If the build manifest couldn't be written,
           * reject the Promise with the Error. */
          writeFile_rejected => reject(writeFile_rejected)
        );
      },

      /* If the source manifest couldn't be read,
       * reject the Promise with the Error. */
      readFile_rejected => reject(readFile_rejected)
    );
  });
}

/**
 * Gracefully build the manifest (handle Promise resolution & rejection internally).
 * @param {ManifestBuilderConfig} config The manifest builder configuration.
 */
function gracefullyBuild(config) {
  build(config).then(
    () => {},
    () => {}
  );
}

/**
 * Watch & gracefully build the manifest.
 * @param {ManifestBuilderConfig} config The manifest builder configuration.
 */
function watch(config) {
  CHOKIDAR.watch(config.srcFile, config.chokidarOpts.watchOpts)
    .on('add', () => gracefullyBuild(config))
    .on('change', () => gracefullyBuild(config))
    .on('unlink', () => gracefullyBuild(config));
}

exports.prebuild = prebuild;
exports.build = build;
exports.watch = watch;
