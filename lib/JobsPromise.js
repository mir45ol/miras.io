/**
 * A Class that resolves or rejects a Promise,
 * once a list of jobs have been resolved or rejected.
 */
class JobsPromise {

  /**
   * Create a JobsPromise.
   * @param {Array.<string>} jobs A list of jobs.
   * @param {Function} resolve A Promise's resolve Function.
   * @param {Function} reject A Promise's reject Function.
   */
  constructor(jobs, resolve, reject) {

    /* Create an empty dictionary of jobs,
     * & store a Promise's resolve & reject Functions. */
    this._jobs = {};
    this._resolve = resolve;
    this._reject = reject;

    /* Populate the dictionary of jobs. */
    jobs.forEach(job => {
      this._jobs[job] = {
        finished: false,
        fulfilled: false,
        rejected: false,
        resolution: undefined,
        rejection: undefined,
      };
    });
  }

  /**
   * Resolve a job.
   * @param {string} job The job.
   * @param {*} resolution The job's resolution.
   */
  resolve(job, resolution) {

    /* Mark the job as finished & fullfilled, & store it's resolution. */
    this._jobs[job].finished = true;
    this._jobs[job].fulfilled = true;
    this._jobs[job].resolution = resolution;

    /* Reevaluate the JobsPromise. */
    this._reevaluate();
  }

  /**
   * Reject a job.
   * @param {string} job The job.
   * @param {*} rejection The job's rejection.
   */
  reject(job, rejection) {

    /* Mark the job as finished & rejected, & store it's rejection. */
    this._jobs[job].finished = true;
    this._jobs[job].rejected = true;
    this._jobs[job].rejection = rejection;

    /* Reevaluate the JobsPromise. */
    this._reevaluate();
  }

  /**
   * Reevaluate if the JobsPromise is finished.
   */
  _reevaluate() {

    /* Track if the JobsPromise is finished, fulfilled, or rejected,
     * & create empty dictionaries of fulfilled & rejected jobs. */
    let
      finished = true,
      fulfilled = true,
      rejected = false,
      fulfilledJobs = {},
      rejectedJobs = {};

    for (const JOB_KEY in this._jobs) {

      /* For each job... */

      const JOB_VAL = this._jobs[JOB_KEY];

      /* If the job isn't finished,
       * mark the JobsPromise as unfinished & break the lööp. */
      finished = finished && JOB_VAL.finished;
      if (!finished) {
        break;
      }

      /* If the job isn't fulfilled, mark the JobsPromise as unfulfilled,
       * else add the job to the dictionary of fulfilled jobs. */
      fulfilled = fulfilled && JOB_VAL.fulfilled;
      if (JOB_VAL.fulfilled) {
        fulfilledJobs[JOB_KEY] = JOB_VAL.resolution;
      }

      /* If the job is rejected, mark the JobsPromise as rejected,
       * & add the job to the dictionary of rejected jobs. */
      rejected = rejected || JOB_VAL.rejected;
      if (JOB_VAL.rejected) {
        rejectedJobs[JOB_KEY] = JOB_VAL.rejection;
      }
    }

    if (finished) {
      if (fulfilled) {

        /* If the JobsPromise is finished & fulfilled,
         * resolve the Promise with the dictionary of fulfilled jobs. */
        this._resolve(fulfilledJobs);

      } else if (rejected) {

        /* If the JobsPromise is finished & rejected,
         * reject the Promise with the dictionary of rejected jobs. */
        this._reject(rejectedJobs);
      }
    }
  }
}

module.exports = JobsPromise;
