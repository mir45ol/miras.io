/* eslint-disable no-console */

const CHALK = require('chalk');

/**
 * Log a message in the following format: `[module] message`.
 * @param {string} module The log's module.
 * @param {string} message The log's message.
 */
function log(module, message) {
  console.log(`[${module}]\t${message}`);
}

/**
 * Log a warning message in the following format: `[module] message`.
 * @param {string} module The log's module.
 * @param {string} message The log's warning message.
 */
function logWarning(module, message) {
  console.warn(CHALK`[${module}]\t{yellow ${message}}`);
}

/**
 * Log a success message in the following format: `[module] message`.
 * @param {string} module The log's module.
 * @param {string} message The log's success message.
 */
function logSuccess(module, message) {
  console.log(CHALK`[${module}]\t{green ${message}}`);
}

/**
 * Log an error message in the following format: `[module] message`.
 * @param {string} module The log's module.
 * @param {string} message The log's error message.
 */
function logError(module, message) {
  console.error(CHALK`[${module}]\t{red ${message}}`);
}

exports.log = log;
exports.logWarning = logWarning;
exports.logSuccess = logSuccess;
exports.logError = logError;
