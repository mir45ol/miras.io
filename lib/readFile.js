const
  FS = require('fs'),
  LOG = require('./log');

/**
 * Read a file.
 * @param {string} logModule The log's module.
 * @param {string|Buffer|URL|number} path The file's path.
 * @returns {Promise} A Promise with a string on resolution & an Error on rejection.
 */
function readFile(logModule, path) {

  /* Return a Promise so we can use JavaScript's async & await features. */
  return new Promise((resolve, reject) => {

    /* Log what we're about to do. */
    LOG.logWarning(logModule, `Reading ${path}`);
    FS.readFile(path, { encoding: 'utf8' }, (err, data) => {
      if (err) {

        /* Log what we couldn't do, the Error's message,
         * & reject the Promise with the Error. */
        LOG.logError(logModule, `Couldn't read ${path}`);
        LOG.logError(err.message);
        reject(err);

      } else {

        /* Log what we did & resolve the Promise with the data. */
        LOG.logSuccess(logModule, `Read ${path}`);
        resolve(data);

      }
    });
  });
}

module.exports = readFile;
