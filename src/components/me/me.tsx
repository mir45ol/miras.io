import m, { VnodeDOM } from "mithril";
import { JsxElement } from "typescript";
import Typed from "typed.js";

class MeComponent {

  public readonly version = m.version;
  public readonly type = "MeComponent";

  public view(): JsxElement {
    return <section class="meComponent">
      <div class="me">
        <img class="icon" src="assets/images/icon-64dp.png"></img>
        <div class="text">
          <h1 class="name"></h1>
          <p class="description"></p>
          <div class="links">

            <a class="link to104px" target="_blank" href="https://gitlab.com/Miras" title="My GitLab">
              <i class="fab fa-gitlab"></i>
              <span>My GitLab</span>
            </a>

            <a class="link to106px" target="_blank" href="https://github.com/M1ras" title="My GitHub">
              <i class="fab fa-github"></i>
              <span>My GitHub</span>
            </a>

            <a class="link to93px" target="_blank" href="mailto:miras@miras.io" title="Email Me">
              <i class="far fa-envelope"></i>
              <span>Email Me</span>
            </a>

          </div>
        </div>
      </div>
    </section>;
  }

  public oncreate(vnode: VnodeDOM) {
    const
      NAME = vnode.dom.getElementsByClassName("name")[0],
      DESCRIPTION = vnode.dom.getElementsByClassName("description")[0];

    new Typed(NAME, {
      strings: [ "Miras Absar" ],
      typeSpeed: 27,
      startDelay: 1000,
      showCursor: false,
      contentType: undefined,
    });

    new Typed(DESCRIPTION, {
      strings: [ "Web & Embedded Systems Engineer." ],
      typeSpeed: 9,
      startDelay: 1000,
      showCursor: false,
      contentType: undefined,
    });
  }
}

export { MeComponent };
