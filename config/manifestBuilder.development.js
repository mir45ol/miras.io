const DIRS = require('./dirs');

module.exports = {
  srcFile: DIRS.srcDir + '/manifest.json',
  buildFile: DIRS.buildDir + '/manifest.json',
  jsonOpts: {
    replacer: undefined,
    space: 2,
  },
  chokidarOpts: {
    paths: DIRS.srcDir + '/manifest.json',
    watchOpts: {},
  },
};
