const DIRS = require('./dirs');

module.exports = {
  srcFile: DIRS.srcDir + '/index.html',
  buildFile: DIRS.buildDir + '/index.html',
  htmlMinifierOpts: {},
  chokidarOpts: {
    paths: DIRS.srcDir + '/index.html',
    watchOpts: {},
  },
};
