const DIRS = require('./dirs');

module.exports = {
  srcFile: DIRS.srcDir + '/index.html',
  buildFile: DIRS.buildDir + '/index.html',
  htmlMinifierOpts: {
    collapseInlineTagWhitespace: true,
    collapseWhitespace: true,
    removeComments: true,
  },
  chokidarOpts: {
    paths: DIRS.srcDir + '/index.html',
    watchOpts: {},
  },
};
