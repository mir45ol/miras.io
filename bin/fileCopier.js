#!/usr/bin/env node

const
  COMMANDER = require('commander'),
  FILE_COPIER = require('../lib/fileCopier'),
  FILE_COPIER_CONFIG = require('../config/fileCopier');

COMMANDER
  .option('-c --copy', 'Copy files.')
  .option('-w --watch', 'Watch files.')
  .parse(process.argv);

main();

function main() {
  if ((process.argv.slice(2).length === 0) ||
      (!COMMANDER.copy && COMMANDER.watch)) {

    COMMANDER.outputHelp();
    process.exit();
  }

  if (!COMMANDER.watch) {

    FILE_COPIER.copy(FILE_COPIER_CONFIG).then(
      () => process.exit(),
      onrejected => process.exit(onrejected.errno || 1)
    );

  }
  else FILE_COPIER.watch(FILE_COPIER_CONFIG);
}
