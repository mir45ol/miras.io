#!/usr/bin/env node

const
  COMMANDER = require('commander'),
  CSS_BUNDLER_DEVELOPMENT_CONFIG = require('../config/cssBundler.development'),
  CSS_BUNDLER_PRODUCTION_CONFIG = require('../config/cssBundler.production'),
  CSS_BUNDLER = require('../lib/cssBundler');

let config;

COMMANDER
  .option('-d --development', 'Build a development bundle.')
  .option('-p --production', 'Build a production bundle.')
  .option('-w --watch', 'Watch files.')
  .parse(process.argv);

main();

async function main() {
  if ((process.argv.slice(2).length === 0) ||
      (!COMMANDER.development && !COMMANDER.production && COMMANDER.watch)) {

    COMMANDER.outputHelp();
    process.exit();
  }

  if (COMMANDER.development) config = CSS_BUNDLER_DEVELOPMENT_CONFIG;
  else if (COMMANDER.production) config = CSS_BUNDLER_PRODUCTION_CONFIG;

  await CSS_BUNDLER.prebuild(config);

  if (!COMMANDER.watch) {

    CSS_BUNDLER.build(config).then(
      () => process.exit(),
      onrejected => process.exit(onrejected.errno || 1)
    );

  } else CSS_BUNDLER.watch(config);
}
