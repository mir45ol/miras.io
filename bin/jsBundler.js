#!/usr/bin/env node

const
  COMMANDER = require('commander'),
  JS_BUNDLER_DEVELOPMENT_CONFIG = require('../config/jsBundler.development'),
  JS_BUNDLER_PRODUCTION_CONFIG = require('../config/jsBundler.production'),
  JS_BUNDLER = require('../lib/jsBundler');

let config;

COMMANDER
  .option('-d --development', 'Build a development bundle.')
  .option('-p --production', 'Build a production bundle.')
  .option('-w --watch', 'Watch files.')
  .parse(process.argv);

main();

async function main() {
  if ((process.argv.slice(2).length === 0) ||
      (!COMMANDER.development && !COMMANDER.production && COMMANDER.watch)) {

    COMMANDER.outputHelp();
    process.exit();
  }

  if (COMMANDER.development) config = JS_BUNDLER_DEVELOPMENT_CONFIG;
  else if (COMMANDER.production) config = JS_BUNDLER_PRODUCTION_CONFIG;

  await JS_BUNDLER.prebuild(config);

  if (!COMMANDER.watch) {

    JS_BUNDLER.build(config).then(
      () => process.exit(),
      onrejected => process.exit(onrejected.errno || 1)
    );

  } else JS_BUNDLER.watch(config);
}
