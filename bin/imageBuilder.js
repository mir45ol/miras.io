#!/usr/bin/env node

const
  COMMANDER = require('commander'),
  IMAGE_BUILDER = require('../lib/imageBuilder'),
  IMAGE_BUILDER_CONFIG = require('../config/imageBuilder');

COMMANDER
  .option('-b --build', 'Build files.')
  .option('-w --watch', 'Watch files.')
  .parse(process.argv);

main();

function main() {
  if ((process.argv.slice(2).length === 0) ||
      (!COMMANDER.build && COMMANDER.watch)) {

    COMMANDER.outputHelp();
    process.exit();
  }

  if (!COMMANDER.watch) {

    IMAGE_BUILDER.build(IMAGE_BUILDER_CONFIG).then(
      () => process.exit(),
      onrejected => process.exit(onrejected.errno || 1)
    );

  } else IMAGE_BUILDER.watch(IMAGE_BUILDER_CONFIG);
}
